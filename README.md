# Docker PHP-FPM 7.2 & Nginx 1.14 on Alpine Linux

Inspired by [TrafeX/docker-php-nginx](https://github.com/TrafeX/docker-php-nginx)

## Build

```bash
docker build -t alpine-nginx-php72 -f Dockerfile .
```

## Contributing

Any type of feedback, pull request or issue is welcome.


## License

The alpine-nginx-php72 docker image is licensed under the MIT license.